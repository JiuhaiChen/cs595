#include <iostream>
#include <cmath>
using namespace std;

static int n = 8;
static double theta = 0.314159265358979323846;



double inner(double a[n][3], double b[n][3], int k)
{
    double sum = 0;
    for(int i=0;i<3;i++)
    {
        sum = a[k][0] * b[k][0] + a[k][1] * b[k][1] +a[k][2] * b[k][2];
    }
    return sum;

}


int main(int argc,char *argv[]) 
{   
  
    double xx[n][3], g[n][3], gc[n][3], b[n][3];
    double mag[n][3],x[3*n];
    double temp1, temp2, temp3, temp4;
    int k;

  /*for(int i=0;i<n;i++)
  {
      for (int j=0;j<3;j++)
      {
          mag[i][j] = 1.0;
          x[3*i+j]  = 1.0;
      }
  }*/

    for(int k=0; k<n; k++)         // the input matrix is (3*N,1), we need to rewrite it into (N,3)
    {
        xx[k][0]=x[3*k];
        xx[k][1]=x[3*k+1];
        xx[k][2]=x[3*k+2];
    }    


     k = 0;
     g[k][0] = 6*mag[k][0]-2*mag[k+1][0];
     g[k][1] = 6*mag[k][1]-2*mag[k+1][1]-4.0;
     g[k][2] = 6*mag[k][2]-2*mag[k+1][2];
     

     temp1 = inner(g,mag,k);
     for(int i=0; i<3 ;i++)
     {
         gc[k][i] = g[k][i] - temp1 * mag[k][i];
     }


     temp1 = inner(mag,xx,k); 
     temp2 = inner(mag,xx,k+1);
     for(int i=0; i<3 ;i++)
     {    
         b[k][i] = 6 * (xx[k][i] - temp1 * mag[k][i]) - 2 * (xx[k+1][i] - temp2 * mag[k+1][i]);
     }
 
     

     temp1 = inner(b,mag,k);
     temp2 = inner(gc,xx,k);
     temp3 = inner(g,mag,k);
     temp4 = inner(xx,mag,k);
     for(int i=0; i<3 ;i++)
     {
         x[3*k+i] = b[k][i] - temp1 * mag[k][i] - temp2 * mag[k][i] - temp3 * (xx[k][i] - temp4 * mag[k][i]) - temp4 * gc[k][i];

     }





     for ( k=1; k<(n-1); k++)
     {
         g[k][0] = 4*mag[k][0]-2*mag[k-1][0]-2*mag[k+1][0];
         g[k][1] = 4*mag[k][1]-2*mag[k-1][1]-2*mag[k+1][1];
         g[k][2] = 4*mag[k][2]-2*mag[k-1][2]-2*mag[k+1][2];
     
         temp1 = inner(g,mag,k);
         for(int i=0; i<3 ;i++)
         {   
             gc[k][i] = g[k][i] - temp1 * mag[k][i];
         }
     
         
         temp1 = inner(mag,xx,k); 
         temp2 = inner(mag,xx,k+1);
         temp3 = inner(mag,xx,k-1);
         for(int i=0; i<3 ;i++)
         {    
             b[k][i] = 4 * (xx[k][i] - temp1 * mag[k][i]) - 2 * (xx[k+1][i] - temp2 * mag[k+1][i]) - 2 * (xx[k-1][i] - temp3 * mag[k-1][i]);
         }
         

         temp1 = inner(b,mag,k);
         temp2 = inner(gc,xx,k);
         temp3 = inner(g,mag,k);
         temp4 = inner(xx,mag,k);
         for(int i=0; i<3 ;i++)
         {
             x[3*k+i] = b[k][i] - temp1 * mag[k][i] - temp2 * mag[k][i] - temp3 * (xx[k][i] - temp4 * mag[k][i]) - temp4 * gc[k][i];

         }
    
     }



     k = n-1;
     g[k][0] = 6*mag[k][0]-2*mag[k-1][0];
     g[k][1] = 6*mag[k][1]-2*mag[k-1][1]-4.0*cos(theta);
     g[k][2] = 6*mag[k][2]-2*mag[k-1][2]-4.0*sin(theta);
     
     temp1 = inner(g,mag,k);
     for(int i=0; i<3 ;i++)
     {
       gc[k][i] = g[k][i] - temp1 * mag[k][i];
     }


     temp1 = inner(mag,xx,k); 
     temp2 = inner(mag,xx,k-1);
     for(int i=0; i<3 ;i++)
     {    
        b[k][i] = 6 * (xx[k][i] - temp1 * mag[k][i]) - 2 * (xx[k-1][i] - temp2 * mag[k-1][i]);
     }
     

     temp1 = inner(b,mag,k);
     temp2 = inner(gc,xx,k);
     temp3 = inner(g,mag,k);
     temp4 = inner(xx,mag,k);
     for(int i=0; i<3 ;i++)
     {
        x[3*k+i] = b[k][i] - temp1 * mag[k][i] - temp2 * mag[k][i] - temp3 * (xx[k][i] - temp4 * mag[k][i]) - temp4 * gc[k][i];

     }

     for(int i=0 ;i<3*n;i++)
     {
       x[i] = n*x[i];
       //cout << x[i]<< endl;
     }
     return 0;
 }

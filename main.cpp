#include <iostream>
#include <cmath>
using namespace std;


static int n=8;
static double theta = 0.314159265358979323846;




double energy(double mag[n][3])
{
double e = 0.0;

e = e + 2 * mag[0][0] * mag[0][0];
e = e + 2 * (mag[0][1]-1) * (mag[0][1]-1);
e = e + 2 * mag[0][2] * mag[0][2];


for(int k = 0; k < n-1; k++)
{
    e = e + (mag[k+1][0] - mag[k][0]) * (mag[k+1][0] - mag[k][0]); 
    e = e + (mag[k+1][1] - mag[k][1]) * (mag[k+1][1] - mag[k][1]);
    e = e + (mag[k+1][2] - mag[k][2]) * (mag[k+1][2] - mag[k][2]);
}


e = e + 2 * mag[n-1][0] * mag[n-1][0];
e = e + 2 * (mag[n-1][1]-cos(theta)) *(mag[n-1][1]-cos(theta));
e = e + 2 * (mag[n-1][2]-sin(theta)) * (mag[n-1][2]-sin(theta));

e = e * n;

return e;

}






void normalization(double mag[n][3])
{
double temp;

for(int i=0; i<n; i++)
{
    temp = sqrt(mag[i][0] * mag[i][0] + mag[i][1] * mag[i][1] + mag[i][2] * mag[i][2]);
    for(int j=0; j<3; j++)
    {
        mag[i][j] = mag[i][j] / temp;
    }
}
}





void gradient(double mag[n][3], double grad[n][3])
{
    double g[n][3];
    int k = 0;
    double temp;


    g[k][0] = 6*mag[k][0] - 2*mag[k+1][0];
    g[k][1] = 6*mag[k][1] - 2*mag[k+1][1] - 4.0;
    g[k][2] = 6*mag[k][2] - 2*mag[k+1][2];

    temp = g[k][0]*mag[k][0]+g[k][1]*mag[k][1]+g[k][2]*mag[k][2];

    for(int i=0;i<3;i++)
    {
       grad[k][i] = g[k][i] - temp*mag[k][i];
    }

    for (k=1;k<n-1;k++)
    {
    g[k][0] = 4*mag[k][0] - 2*mag[k-1][0] - 2*mag[k+1][0];
    g[k][1] = 4*mag[k][1] - 2*mag[k-1][1] - 2*mag[k+1][1];
    g[k][2] = 4*mag[k][2] - 2*mag[k-1][2] - 2*mag[k+1][2];

    temp = g[k][0]*mag[k][0]+g[k][1]*mag[k][1]+g[k][2]*mag[k][2];

    for(int i=0;i<3;i++)
    {
       grad[k][i] = g[k][i] - temp*mag[k][i];
    }

    }

    k = n-1;
    g[k][0] = 6*mag[k][0] - 2*mag[k-1][0];
    g[k][1] = 6*mag[k][1] - 2*mag[k-1][1] - 4*cos(theta);
    g[k][2] = 6*mag[k][2] - 2*mag[k-1][2] - 4*sin(theta);

    temp = g[k][0]*mag[k][0]+g[k][1]*mag[k][1]+g[k][2]*mag[k][2];

    for(int i=0;i<3;i++)
    {
       grad[k][i] = g[k][i] - temp*mag[k][i];
    }


     for(int i=0;i<n;i++)
    {
        for(int j=0;j<3;j++)
        {
            grad[i][j] = n * grad[i][j];
        }
    }

}






double inf_norm( double grad[n][3])
{
    double norm = 0.0;

    for(int i=0;i<n;i++)
    {
        for(int j=0;j<3;j++)
        {
            norm = norm + grad[i][j] * grad[i][j];
        }
    }
    norm = sqrt(norm);

    return norm;

}




void direction(double grad[n][3], double direct[n][3])
{

for(int i=0; i<n; i++)
{
    for(int j=0; j<3; j++)
    {
        direct[i][j] = - grad[i][j];
    }
}
}






void linesearch(double mag[n][3], double direct[n][3], double grad[n][3])
{

  double z0[n][3], z[n][3];
  double gu[n][3];
  double eps = 1e-4;
  int kmax = 20, kcount = 1;
  double f0, g0, gp;

  f0 = energy(mag);
  g0 = f0;

  gp = 0.0;

  for(int i=0; i<n; i++)
  {
    for(int j=0; j<3; j++)
    {   
        gp = gp + grad[i][j] * direct[i][j];
    }
  }
   

  if (gp>0) 
  {  
    gp = -gp;
    for(int i=0; i<n; i++)
    {
      for(int j=0; j<3; j++)
      {
        direct[i][j] = -direct[i][j];
      }
    }

  }



  double rlambda2 = 1.0, rlambda1 = 1.0, rlambda0 = 0.0;
  double e0, f1, f2;
  e0 = f0;
  f1 = f0;

  for(int i=0;i<n;i++)
  { 
      for(int j=0;j<3;j++)
     {
        z0[i][j] = mag[i][j] + rlambda2 * direct[i][j];
        
     }
  }

  normalization(z0);
  
 
  f2 = energy(z0);
  



  while (f2>(e0+eps*rlambda2*gp) && (kcount<kmax))
  {
      if (kcount==1)
      {
          rlambda0 = rlambda1;
          rlambda1 = rlambda2;
          rlambda2 = max(-gp/(2*(f2-e0-gp)),0.1);
          f0 = f1;
          f1 = f2; 
      }  
      else
      {
          rlambda0 = rlambda1;
          rlambda1 = rlambda2;
          double a = ((f1-e0-gp*rlambda1)/(rlambda1*rlambda1)-(f0-e0-gp*rlambda0)/(rlambda0*rlambda0))/(rlambda1-rlambda0);
          double b = ((f1-e0-gp*rlambda1)*(-rlambda0/(rlambda1*rlambda1))-(f0-e0-gp*rlambda0)*(rlambda1/(rlambda0*rlambda0)))/(rlambda1-rlambda0);
          double disc = 1.0-3.0*a*gp/(b*b);
          if (3.0*a == 0.0)
          {
              rlambda2 = gp/(2.0*b);
          }
          else
          {
              rlambda2 = (-b+abs(b)*sqrt(disc))/(3.0*a);
          }
     
          if (rlambda2>(0.5*rlambda1))
          {
            rlambda2 = 0.5*rlambda1;
          }
            else if (rlambda2<(0.1*rlambda1))
            {
                rlambda2 = 0.1*rlambda1;
            }
          
         f0 = f1;
         f1 = f2;
         } 
    
    
  

      for(int i=0;i<n;i++)
      {
          for(int j=0;j<3;j++)
          {   
              z0[i][j] = mag[i][j] + rlambda2 * direct[i][j];
          }
      }
     
      
    
      normalization(z0);
      f2 = energy(z0);
      kcount = kcount + 1;
      
    }
    
     for(int i=0;i<n;i++)
      {
          for(int j=0;j<3;j++)
          {   
              mag[i][j] = z0[i][j];
          }
      }
   
}











int main(int argc,char *argv[]) 
{

double grid, niter, energy = 0.0, norm = 0.0 ;
int count = 0;

double mag[n][3];
double direct[n][3];
double grad[n][3];

 for(int i=0;i<n;i++)
    {
        for(int j=0;j<3;j++)
        {
             mag[i][j] = 1;
        }
    }

normalization(mag);
gradient(mag, grad);
norm = inf_norm(grad);



  while(norm>1e-4)
   {   
       
   direction(grad,direct);
   linesearch(mag,direct,grad);

   normalization(mag);
   gradient(mag,grad);
   norm = inf_norm(grad);
   count = count + 1;

  }   
   for(int i=0;i<n;i++)
    {
        for(int j=0;j<3;j++)
        {
              cout << mag[i][j]<< endl;
        }
    }
 

  return 0;

}
ALL:WASH
CFLAGS	         = 
FFLAGS	         = 
CPPFLAGS         = 
FPPFLAGS         =
EXAMPLESC        =
LOCDIR           = src/ts/examples/tutorials/network/wash/

#OBJECTS_PIPE = pipeInterface.o pipeImpls.o

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

main: main.o chkopts
	-${CLINKER} -o main main.o ${PETSC_TS_LIB}
	${RM} main.o

ex7: ex7.o chkopts
	-${CLINKER} -o ex7 ex7.o ${PETSC_TS_LIB}
	${RM} ex7.o

include ${PETSC_DIR}/lib/petsc/conf/test

function [e] = energy(m,N,theta)
e=0;

k = 1;
e = e + 2 * (m(1,k)^2+(m(2,k)-1)^2 + m(3,k)^2);

for k = 1:(N-1)
    e = e + ( (m(1,k+1)-m(1,k))^2 + (m(2,k+1)-m(2,k))^2 + ...
        (m(3,k+1)-m(3,k))^2 );
end

k = N;
e = e + 2 * (m(1,k)^2+(m(2,k)-cos(theta))^2 + (m(3,k)-sin(theta))^2);

e = e * N;
end
        
    
clear all;clc;
format long;
Nrefine = 4;
Grid = zeros(1,Nrefine);
Niter = zeros(1,Nrefine);
Ene = zeros(1,Nrefine);
Eneref = 0.0986959951274858;

for j = 1:Nrefine
    tic;
    N = 4*2^j;
    Grid(1,j) = 1.0 / N;
    count = 0;
    theta = pi*0.1;
    m0 = zeros(3,N);
    m = zeros(3,N);
    mref = zeros(3,N);
    d = zeros(3,N);
    
    for k=1:N
        m(:,k) = rand(3,1);
        mref(2,k) = 1.0;
    end
    m0 = normalization(m,N);
    
    d = -gradient(m0,N,theta);
    
    while norm(d,inf)>1e-4
        [m rlambda2 kcount f0] = linesearch(m0,d,N,theta);
        m0 = normalization(m,N);
        d = -gradient(m0,N,theta);
        count = count + 1;
    end

    Niter(1,j) = count;
    Ene(1,j) = f0;
    toc;
end

figure(1);clf;
axes('FontSize',20);
loglog(Grid,abs(Ene-Eneref),'.','MarkerSize',18);
hold on;
loglog(Grid,Grid.^2,'-','LineWidth',2);
legend('Numerical','h^2')
xlabel('h','FontSize',20)
ylabel('Energy error','FontSize',20)
%print -depsc energyerror -r1500
%print -dpdf energyerror -r300
y1=polyfit(log(Grid),log(abs(Ene-Eneref)),1)
disp('Iteration numbers of the steepest descent method for convergence');
Grid
Niter

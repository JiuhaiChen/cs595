function [u] = normalization(m,N)

u = zeros(size(m));

for k = 1:N
    u(:,k) = m(:,k)/norm(m(:,k),2);
end

end

function [u rlambda2 kcount f0] = linesearch(m,d,N,theta)

z0 = zeros(size(m));
z = zeros(size(m));
gu = zeros(size(m));

eps = 1e-4;
kmax = 20;
kcount = 1;
f0 = energy(m,N,theta);

g0 = f0;

gu = gradient(m,N,theta);
gp = 0.0;
for k = 1:N
    gp = gp + gu(:,k)'*d(:,k);
end

if gp>0
    d = -d;
    gp = -gp;
end

rlambda2 = 1.0;
rlambda1 = 1.0;

e0 = f0;
f1 = f0;

z0 = m + rlambda2*d;
z = normalization(z0,N);
f2 = energy(z,N,theta);

while (f2>(e0+eps*rlambda2*gp))&&(kcount<kmax)

    if kcount==1
        rlambda0 = rlambda1;
        rlambda1 = rlambda2;
        rlambda2 = max(-gp/(2*(f2-e0-gp)),0.1);
        f0 = f1;
        f1 = f2;
    else
        rlambda0 = rlambda1;
        rlambda1 = rlambda2;
        a = ((f1-e0-gp*rlambda1)/(rlambda1^2)-(f0-e0-gp*rlambda0)/(rlambda0^2))/(rlambda1-rlambda0);
        b = ((f1-e0-gp*rlambda1)*(-rlambda0/rlambda1^2)-(f0-e0-gp*rlambda0)*(rlambda1/rlambda0^2))/(rlambda1-rlambda0);
        disc = 1.0-3.0*a*gp/b^2;
        if 3.0*a == 0.0
            rlambda2 = gp/(2.0*b);
        else
            rlambda2 = (-b+abs(b)*sqrt(disc))/(3.0*a);
        end
        if rlambda2>(0.5*rlambda1)
            rlambda2 = 0.5*rlambda1
        else if (rlambda2<(0.1*rlambda1))
                 rlambda2 = 0.1*rlambda1;
            end
        end
        f0 = f1;
        f1 = f2;
    end
    
    z0 = m + rlambda2*d;
    z = normalization(z0,N);
    f2 = energy(z,N,theta);
    kcount = kcount+1;
end    

u = z;
f0 = f2;
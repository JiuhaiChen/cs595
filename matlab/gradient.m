function [gc] = gradient(m,N,theta) 

g = zeros(size(m));
gc = zeros(size(m));

k = 1;
g(1,k) = 6*m(1,k) - 2*m(1,k+1);
g(2,k) = 6*m(2,k) - 2*m(2,k+1) - 4.0;
g(3,k) = 6*m(3,k) - 2*m(3,k+1);
gc(:,k) = g(:,k) - (g(:,k)'*m(:,k))*m(:,k);

for k=2:(N-1)
    g(1,k) = 4*m(1,k) - 2*m(1,k-1) - 2*m(1,k+1);
    g(2,k) = 4*m(2,k) - 2*m(2,k-1) - 2*m(2,k+1);
    g(3,k) = 4*m(3,k) - 2*m(3,k-1) - 2*m(3,k+1);
    gc(:,k) = g(:,k) - (g(:,k)'*m(:,k))*m(:,k);
end

k = N;
g(1,k) = 6*m(1,k) - 2*m(1,k-1);
g(2,k) = 6*m(2,k) - 2*m(2,k-1) - 4*cos(theta);
g(3,k) = 6*m(3,k) - 2*m(3,k-1) - 4*sin(theta);
gc(:,k) = g(:,k) - (g(:,k)'*m(:,k))*m(:,k);

g = g * N;
gc = gc * N;

end
        